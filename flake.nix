{
    description = "An extremely basic flake";

    outputs = { self, nixpkgs }: 
        let pkgs = nixpkgs.legacyPackages.x86_64-darwin; in
        with pkgs;
        {
            defaultPackage = {
                "x86_64-darwin" = derivation {
                    name = "dobber";
                    system = "x86_64-darwin";
                    builder = "${bash}/bin/bash";
                    args = [ ./dob.sh self self.sourceInfo self.lastModifiedDate  ];
                };
            };
        };
}
