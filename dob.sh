echo My location: > $out
echo $(pwd) >> $out

# Report calling args:
echo Calling args: >> $out
echo $@ >> $out

# Copy all environmental variables into the output
echo Environmental variables: >> $out
export >> $out
